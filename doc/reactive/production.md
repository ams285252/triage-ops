# Production

## How we run on Kubernetes

The [`triage-ops-prod` Kubernetes cluster](https://console.cloud.google.com/kubernetes/clusters/details/us-central1/triage-ops-prod/details?project=gitlab-qa-resources)
is in the project [gitlab-qa-resources](https://console.cloud.google.com/kubernetes/list?project=gitlab-qa-resources).

This is a Rack application which processes the webhooks.

0. <https://triage-ops.gitlab.com/> Resolve [DNS records](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14764) to IP pointing to the `ingress-nginx-controller` Service:

       kubectl describe service ingress-nginx-controller --namespace=ingress-nginx

0. Then where the traffic goes?
0. Then traffic goes to `triage-web-ingress` since it has the `kubernetes.io/ingress.class: "nginx"` annotation

       kubectl describe ingress -l app=triage-web --all-namespaces

0. Then traffic goes to `triage-web-service` thanks to `rules[0].http.paths[0].backend.serviceName: triage-web-service`

       kubectl describe service -l app=triage-web --all-namespaces

Note: Both `triage-web-ingress` and `triage-web-service` are part of `triage-web-deployment`:

       kubectl describe deployment -l app=triage-web --all-namespaces

#### `event.from_gitlab_org?` as feature flags

We can use `event.from_gitlab_org?` to block events from
[`gitlab-org`](https://gitlab.com/gitlab-org), and deploy to production,
and then test in
[triage-test](https://gitlab.com/gl-quality/eng-prod/triage-test).
This way, we can try out the new feature in a separate project before
applying it to `gitlab-org`.

After enough tests, we can then create a merge request and remove the block,
actually applying to `gitlab-org`.

#### Setting up `kubectl` for production access

* Install `gcloud`

      brew cask install google-cloud-sdk

* Install `kubectl`

      brew install kubernetes-cli

* Log in to GCK

      gcloud auth login

* Configure `kubectl`

      gcloud container clusters get-credentials triage-ops-prod --zone us-central1 --project gitlab-qa-resources

* Run local proxy

      kubectl proxy

# Production monitoring

- [Triage reactive overview dashboard](https://console.cloud.google.com/monitoring/dashboards/builder/3a38bbf5-41bc-46c9-a36b-fe23cafb6dc8?project=gitlab-qa-resources&dashboardBuilderState=%257B%2522editModeEnabled%2522:false%257D&timeDomain=1h&ref=https:%2F%2Fconsole.cloud.google.com%2Fmonitoring%2Fdashboards%2Fbuilder%2F3a38bbf5-41bc-46c9-a36b-fe23cafb6dc8%3Fproject%3Dgitlab-qa-resources%26dashboardBuilderState%3D%25257B%252522editModeEnabled%252522:false%25257D%26timeDomain%3D1w)
- [Logs explorer](https://cloudlogging.app.goo.gl/EUPCuUQFj6NdB5AY6)
- [Sentry project](https://sentry.gitlab.net/gitlab/triage-ops/)
