# frozen_string_literal: true

require_relative '../triage/triage/event'

module CommunityContributionHelper
  GITLAB_DEPENDENCY_UPDATE_BOT = 'gitlab-dependency-update-bot'

  def bot_author?
    automation_bot? || project_group_bot?
  end

  private

  def automation_bot?
    author == GITLAB_DEPENDENCY_UPDATE_BOT
  end

  def project_group_bot?
    author.match?(::Triage::Event::GITLAB_SERVICE_ACCOUNT_REGEX)
  end
end
